Multi-monitor Alt-Tab
---------------------
This test only applies for multiple monitor.
App names here are an example, any apps will do.

Setup:
#. Start with no apps open (or at least remember what is open where)
#. Start terminal on workspace 1
#. Start firefox on workspace 2
#. Move to workspace 2

Scenario 1: Perform on workspace 1
Scenario 2: Perform on workspace 2

Action:
#. Hold Alt, press tab
#. Observe
#. Release Alt

Scenario 1 Outcome:
  Terminal is shown in the alt-tab switcher, but firefox on workspace 2 is not.
Scenario 2 Outcome:
  Firefox is shown in the alt-tab switcher, but the terminal on workspace 1 is not.


Panel on Alt+Tab
----------------
This tests that the panel title changes with Alt+Tab

Setup:
#. Open some new applications

Action:
#. Press Alt+Tab

Outcome
  When you press Alt+Tab to switch window, the application title in the top left
  of the panel should change inline with changes in alt-tab focus.
  Also the menus shouldn't ever show. Even if the mouse has been left over
  the panel when starting the Alt+Tab.

Full app title in Chinese
-------------------------
This test ensures that the App Switcher (Alt+Tab) displays the full title
of apps in Chinese.

Setup:
#. Install gnome-tweak-tool and run it.
#. Make sure you have the default font scaling factor (1.0).

Action:
#. Run:
   python -c 'from gi.repository import Gtk;win=Gtk.Window(title="显示桌面");win.connect("destroy", Gtk.main_quit);win.show_all();Gtk.main();'
#. Press Alt+Tab.
#. Continue to press Tab to select the newly created window.

Outcome
  The title of the newly created window should be "显示桌面" and *not* "显示桌...".


Alt+keyabovetab switching
------------------------------
This test ensures that alt+keyabovetab switching windows of the application
works as it should.

Setup:
#. Have a few windows of one application open (e.g. 3 terminal windows).

Action:
#. Focus one of the opened application windows.
#. Press Alt+keyabovetab (the ~ key on the US and PL keyboard).
#. Keep the Alt key pressed.

Outcome
  The switcher should appear with the detail view for the given application
  (e.g. the terminal), with all terminal applications for the workspace 
  visible. The switcher should look normally.


Fast alt+keyabovetab switching
------------------------------
This test ensures that doing a fast tap does not result in any 'flickering' of
the switcher window. The switcher should not appear during a quick tap, just
during longer alt+keyabovetab presses.

Setup:
#. Have a few windows of one application open (e.g. 3 terminal windows)

Action:
#. Focus one of the opened application windows.
#. Quickly tap Alt+keyabovetab (meaning only tap once, do not hold)
#. Repeat at least a few times

Outcome
  On every quick tap, the focus should switch to the next same-application
  type window. During that switch the switcher should not appear, not even
  for a brief moment (if done fast enough). No 'flickering' of the switcher.


Alt+Tab doesn't lose window focus
---------------------------------
This tests ensures that a racing condition is not possible anymore when alt+tabing.

Setup:
#. Have 2 windows of different types open. (gedit, nautilus)

Actions:
  #. Press quicky Alt+Tab. (so that the switcher window is not shown)
  #. Repeat 10 times.

Expected Results:
  No window loses focuses at all. The panel should alway show the title of the
  window.
